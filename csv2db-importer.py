# encoding: utf-8
'''
csv2db-importer imports CSV files to your databases (SQLite, MySQL)
'''
from __future__ import unicode_literals, print_function, absolute_import
import sys
import logging

__author__ = "Ozan HACIBEKİROĞLU"
__version__ = "0.1a1"

if __name__ == "__main__":
    if "--console" in sys.argv or "-c" in sys.argv:
        from csv2dbimporter.console import MainConsole
        from csv2dbimporter.exitcodes import EXIT_UNIDENTIFIED_ERROR
        app = MainConsole(sys.argv[1:],)
        returncode = app.run()
        logger = logging.getLogger("csv2db")
        logger.debug("Exit Code:%s" % returncode)
        try:
            sys.exit(int(returncode))
        except ValueError:
            sys.exit(EXIT_UNIDENTIFIED_ERROR)
    else:
        import wx
        from csv2dbimporter.maingui import MainFrame
        app = wx.App(True)
        frame = MainFrame()
        app.MainLoop()
