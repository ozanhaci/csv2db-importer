# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jun 17 2015)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

from .mylib import CustomColumnGrid
import wx
import wx.xrc
import wx.grid

###########################################################################
## Class BaseMainFrame
###########################################################################

class BaseMainFrame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"csv2db-importer", pos = wx.DefaultPosition, size = wx.Size( 780,775 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizerFrame = wx.BoxSizer( wx.VERTICAL )
		
		self.m_scrolledWindowMain = wx.ScrolledWindow( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.HSCROLL|wx.VSCROLL )
		self.m_scrolledWindowMain.SetScrollRate( 5, 5 )
		bSizerScrolledWindow = wx.BoxSizer( wx.VERTICAL )
		
		self.m_panelTop = wx.Panel( self.m_scrolledWindowMain, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		gbSizerTop = wx.GridBagSizer( 0, 0 )
		gbSizerTop.SetFlexibleDirection( wx.BOTH )
		gbSizerTop.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		gbSizerTop.SetEmptyCellSize( wx.Size( 0,0 ) )
		
		self.m_infoCtrlMain = wx.InfoBar( self.m_panelTop )
		self.m_infoCtrlMain.SetShowHideEffects( wx.SHOW_EFFECT_SLIDE_TO_BOTTOM, wx.SHOW_EFFECT_SLIDE_TO_TOP )
		self.m_infoCtrlMain.SetEffectDuration( 500 )
		self.m_infoCtrlMain.SetMinSize( wx.Size( 350,-1 ) )
		
		gbSizerTop.Add( self.m_infoCtrlMain, wx.GBPosition( 0, 1 ), wx.GBSpan( 1, 5 ), wx.ALL|wx.EXPAND, 0 )
		
		self.m_staticTextProgName = wx.StaticText( self.m_panelTop, wx.ID_ANY, u"csv2db-importer", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticTextProgName.Wrap( -1 )
		self.m_staticTextProgName.SetFont( wx.Font( 20, 70, 90, 92, False, wx.EmptyString ) )
		
		gbSizerTop.Add( self.m_staticTextProgName, wx.GBPosition( 0, 0 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )
		
		
		self.m_panelTop.SetSizer( gbSizerTop )
		self.m_panelTop.Layout()
		gbSizerTop.Fit( self.m_panelTop )
		bSizerScrolledWindow.Add( self.m_panelTop, 1, wx.ALL|wx.EXPAND, 0 )
		
		self.m_panelBottom = wx.Panel( self.m_scrolledWindowMain, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizerNotebook = wx.BoxSizer( wx.VERTICAL )
		
		self.m_notebook = wx.Notebook( self.m_panelBottom, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_panelPage1 = wx.Panel( self.m_notebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		gbSizerPage1 = wx.GridBagSizer( 0, 0 )
		gbSizerPage1.SetFlexibleDirection( wx.BOTH )
		gbSizerPage1.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticTextDateFormat = wx.StaticText( self.m_panelPage1, wx.ID_ANY, u"Date Format", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticTextDateFormat.Wrap( -1 )
		gbSizerPage1.Add( self.m_staticTextDateFormat, wx.GBPosition( 1, 1 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_textCtrlDateFormat = wx.TextCtrl( self.m_panelPage1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textCtrlDateFormat.SetToolTipString( u"Python compatible date-time formats are valid.\n%Y is 4 digit year\n%m is 2 digit month\n%d is 2 digit day\netc." )
		
		gbSizerPage1.Add( self.m_textCtrlDateFormat, wx.GBPosition( 1, 2 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticTextTimeFormat = wx.StaticText( self.m_panelPage1, wx.ID_ANY, u"Time Format", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticTextTimeFormat.Wrap( -1 )
		gbSizerPage1.Add( self.m_staticTextTimeFormat, wx.GBPosition( 2, 1 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_textCtrlTimeFormat = wx.TextCtrl( self.m_panelPage1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textCtrlTimeFormat.SetToolTipString( u"Python compatible date-time formats are valid\n%H is Hour (24-Hour clock and zero padded)\n%I is Hour (12-Hour clock and zero padded)\n%M is Minute (zero padded)\n%S is Second (zero padded)\netc." )
		
		gbSizerPage1.Add( self.m_textCtrlTimeFormat, wx.GBPosition( 2, 2 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticTextDTSep = wx.StaticText( self.m_panelPage1, wx.ID_ANY, u"Date Time Separator", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticTextDTSep.Wrap( -1 )
		gbSizerPage1.Add( self.m_staticTextDTSep, wx.GBPosition( 3, 1 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )
		
		m_comboBoxDTSepChoices = [ u"<space>" ]
		self.m_comboBoxDTSep = wx.ComboBox( self.m_panelPage1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, m_comboBoxDTSepChoices, 0 )
		self.m_comboBoxDTSep.SetToolTipString( u"Select from combobox or type a date time separator character" )
		
		gbSizerPage1.Add( self.m_comboBoxDTSep, wx.GBPosition( 3, 2 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticTextColumnSeparator = wx.StaticText( self.m_panelPage1, wx.ID_ANY, u"Column Separator", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticTextColumnSeparator.Wrap( -1 )
		gbSizerPage1.Add( self.m_staticTextColumnSeparator, wx.GBPosition( 4, 1 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		m_comboBoxColSepChoices = [ u"<comma>", u"<semicolon>", u"<tab>", u"<space>", u"<pipe>" ]
		self.m_comboBoxColSep = wx.ComboBox( self.m_panelPage1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, m_comboBoxColSepChoices, 0 )
		self.m_comboBoxColSep.SetToolTipString( u"Select a delimiter used to separate columns/fields." )
		
		gbSizerPage1.Add( self.m_comboBoxColSep, wx.GBPosition( 4, 2 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticTextStrDel = wx.StaticText( self.m_panelPage1, wx.ID_ANY, u"String Delimiter", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticTextStrDel.Wrap( -1 )
		gbSizerPage1.Add( self.m_staticTextStrDel, wx.GBPosition( 5, 1 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		m_comboBoxStrDelChoices = [ u"<double quotation>", u"<single quotation>" ]
		self.m_comboBoxStrDel = wx.ComboBox( self.m_panelPage1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, m_comboBoxStrDelChoices, 0 )
		self.m_comboBoxStrDel.SetToolTipString( u"Select a character that surrounds a string value." )
		
		gbSizerPage1.Add( self.m_comboBoxStrDel, wx.GBPosition( 5, 2 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticTextDecPoint = wx.StaticText( self.m_panelPage1, wx.ID_ANY, u"Decimal Point", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticTextDecPoint.Wrap( -1 )
		gbSizerPage1.Add( self.m_staticTextDecPoint, wx.GBPosition( 6, 1 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		m_comboBoxDecPointChoices = [ u"<point>", u"<comma>" ]
		self.m_comboBoxDecPoint = wx.ComboBox( self.m_panelPage1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, m_comboBoxDecPointChoices, 0 )
		self.m_comboBoxDecPoint.SetToolTipString( u"Must be defined if decimal amounts present." )
		
		gbSizerPage1.Add( self.m_comboBoxDecPoint, wx.GBPosition( 6, 2 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticTextThousandSep = wx.StaticText( self.m_panelPage1, wx.ID_ANY, u"Thousand Separator", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticTextThousandSep.Wrap( -1 )
		gbSizerPage1.Add( self.m_staticTextThousandSep, wx.GBPosition( 7, 1 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		m_comboBoxThousandSepChoices = [ u"<point>", u"<comma>" ]
		self.m_comboBoxThousandSep = wx.ComboBox( self.m_panelPage1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, m_comboBoxThousandSepChoices, 0 )
		self.m_comboBoxThousandSep.SetToolTipString( u"Separator between thousands in the amount fields." )
		
		gbSizerPage1.Add( self.m_comboBoxThousandSep, wx.GBPosition( 7, 2 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticTextCSVPath = wx.StaticText( self.m_panelPage1, wx.ID_ANY, u"CSV File Path", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticTextCSVPath.Wrap( -1 )
		gbSizerPage1.Add( self.m_staticTextCSVPath, wx.GBPosition( 8, 1 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_filePickerCSV = wx.FilePickerCtrl( self.m_panelPage1, wx.ID_ANY, wx.EmptyString, u"Select CSV File", u"*.csv", wx.DefaultPosition, wx.DefaultSize, wx.FLP_DEFAULT_STYLE )
		self.m_filePickerCSV.SetToolTipString( u"Select a CSV file that is valid according to your settings above." )
		
		gbSizerPage1.Add( self.m_filePickerCSV, wx.GBPosition( 8, 2 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticTextCSVEnc = wx.StaticText( self.m_panelPage1, wx.ID_ANY, u"CSV File Encoding", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticTextCSVEnc.Wrap( -1 )
		gbSizerPage1.Add( self.m_staticTextCSVEnc, wx.GBPosition( 9, 1 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		m_comboBoxCSVFileEncChoices = [ u"UTF-8" ]
		self.m_comboBoxCSVFileEnc = wx.ComboBox( self.m_panelPage1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, m_comboBoxCSVFileEncChoices, 0 )
		self.m_comboBoxCSVFileEnc.SetToolTipString( u"CSV File encoding is guessed (not 100% correct) after file is selected. If guessing is not successful you should type or select file encoding yourself. If file encoding is not typed or selected, \"UTF-8\" will be considered as default encoding." )
		
		gbSizerPage1.Add( self.m_comboBoxCSVFileEnc, wx.GBPosition( 9, 2 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.EXPAND, 5 )
		
		self.m_checkBoxCSVEncAutoDetected = wx.CheckBox( self.m_panelPage1, wx.ID_ANY, u"Auto Detected", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_checkBoxCSVEncAutoDetected.Enable( False )
		
		gbSizerPage1.Add( self.m_checkBoxCSVEncAutoDetected, wx.GBPosition( 9, 3 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_checkBoxForceCSVEncDetect = wx.CheckBox( self.m_panelPage1, wx.ID_ANY, u"Force CSV File Encoding Detection", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_checkBoxForceCSVEncDetect.SetToolTipString( u"Athough CSV file encoding is given, try to detect file encoding automatically." )
		
		gbSizerPage1.Add( self.m_checkBoxForceCSVEncDetect, wx.GBPosition( 10, 2 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )
		
		self.m_checkBoxSkipFirstRow = wx.CheckBox( self.m_panelPage1, wx.ID_ANY, u"Skip First Row", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_checkBoxSkipFirstRow.SetToolTipString( u"You should check \"Skip First Row\" if your CSV file's first line contains headings (not records)." )
		
		gbSizerPage1.Add( self.m_checkBoxSkipFirstRow, wx.GBPosition( 11, 2 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		self.m_panelPage1.SetSizer( gbSizerPage1 )
		self.m_panelPage1.Layout()
		gbSizerPage1.Fit( self.m_panelPage1 )
		self.m_notebook.AddPage( self.m_panelPage1, u"CSV Settings", True )
		self.m_panelPage2 = wx.Panel( self.m_notebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		gbSizerPage2 = wx.GridBagSizer( 0, 0 )
		gbSizerPage2.SetFlexibleDirection( wx.BOTH )
		gbSizerPage2.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticTextSQLiteDBPath = wx.StaticText( self.m_panelPage2, wx.ID_ANY, u"SQLite Database File Path", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticTextSQLiteDBPath.Wrap( -1 )
		gbSizerPage2.Add( self.m_staticTextSQLiteDBPath, wx.GBPosition( 0, 1 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_filePickerSQLitePath = wx.FilePickerCtrl( self.m_panelPage2, wx.ID_ANY, wx.EmptyString, u"Select an SQLite Database", u"*.*", wx.DefaultPosition, wx.DefaultSize, wx.FLP_DEFAULT_STYLE )
		self.m_filePickerSQLitePath.SetToolTipString( u"If target database is SQLite, select the database file and let it create the database connection url" )
		
		gbSizerPage2.Add( self.m_filePickerSQLitePath, wx.GBPosition( 0, 2 ), wx.GBSpan( 1, 5 ), wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticTextDbConUrl = wx.StaticText( self.m_panelPage2, wx.ID_ANY, u"Database Connection URL", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticTextDbConUrl.Wrap( -1 )
		gbSizerPage2.Add( self.m_staticTextDbConUrl, wx.GBPosition( 1, 1 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_textCtrlDbConUrl = wx.TextCtrl( self.m_panelPage2, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( -1,-1 ), 0 )
		self.m_textCtrlDbConUrl.SetToolTipString( u"This must be valid SQLAlchemy database connection url.\nIf you select an SQLite database it automatically creates the url.\nFor other databases, please check the SQLAlchemy Engine Configuration." )
		
		gbSizerPage2.Add( self.m_textCtrlDbConUrl, wx.GBPosition( 1, 2 ), wx.GBSpan( 1, 5 ), wx.ALL|wx.EXPAND, 5 )
		
		self.m_checkBoxInsert = wx.CheckBox( self.m_panelPage2, wx.ID_ANY, u"Insert", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_checkBoxInsert.SetToolTipString( u"Check to enable creating new records in database using CSV file." )
		
		gbSizerPage2.Add( self.m_checkBoxInsert, wx.GBPosition( 1, 7 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_checkBoxUpdate = wx.CheckBox( self.m_panelPage2, wx.ID_ANY, u"Update", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_checkBoxUpdate.SetToolTipString( u"Check to enable updating current records in database using CSV file. Update operations are handled using primary keys checked as Primary Key in the grid below." )
		
		gbSizerPage2.Add( self.m_checkBoxUpdate, wx.GBPosition( 1, 8 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_staticTextTableName = wx.StaticText( self.m_panelPage2, wx.ID_ANY, u"Table Name", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticTextTableName.Wrap( -1 )
		gbSizerPage2.Add( self.m_staticTextTableName, wx.GBPosition( 2, 1 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )
		
		m_comboBoxTableNameChoices = []
		self.m_comboBoxTableName = wx.ComboBox( self.m_panelPage2, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, m_comboBoxTableNameChoices, 0 )
		self.m_comboBoxTableName.SetToolTipString( u"Target table name in database.\nTable must exist in database before importing." )
		
		gbSizerPage2.Add( self.m_comboBoxTableName, wx.GBPosition( 2, 2 ), wx.GBSpan( 1, 5 ), wx.ALL|wx.EXPAND, 5 )
		
		self.m_buttonGetTableDef = wx.Button( self.m_panelPage2, wx.ID_ANY, u"Get Table Definition", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_buttonGetTableDef.SetToolTipString( u"Tries to fetch columns' definitions using the database connection url and table name. If table name does not present, it fetches tables' names." )
		
		gbSizerPage2.Add( self.m_buttonGetTableDef, wx.GBPosition( 2, 7 ), wx.GBSpan( 1, 2 ), wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_buttonDBGridAdd = wx.Button( self.m_panelPage2, wx.ID_ANY, u"Add Grid Row", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_buttonDBGridAdd.SetToolTipString( u"Add new row to the grid so new column definition can be created." )
		
		gbSizerPage2.Add( self.m_buttonDBGridAdd, wx.GBPosition( 4, 2 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )
		
		self.m_buttonSortDBGrid = wx.Button( self.m_panelPage2, wx.ID_ANY, u"Sort Grid by Order", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_buttonSortDBGrid.SetToolTipString( u"Import process maps CSV columns with table columns using columns orders defined below. You can change columns' orders and you can sort them manually by pressing this button." )
		
		gbSizerPage2.Add( self.m_buttonSortDBGrid, wx.GBPosition( 4, 3 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )
		
		self.m_buttonDBRowDel = wx.Button( self.m_panelPage2, wx.ID_ANY, u"Delete Grid Row", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_buttonDBRowDel.SetToolTipString( u"You must select row(s) by clicking on row labels before deleting.\nColumns which are not in the grid, are not processed during import process." )
		
		gbSizerPage2.Add( self.m_buttonDBRowDel, wx.GBPosition( 4, 4 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )
		
		self.m_staticTextDBInfoText = wx.StaticText( self.m_panelPage2, wx.ID_ANY, u"Columns in CSV file and Columns defined below are mapped using the order number of the columns below.\nBefore importing to database \"Sort Grid by Order\" is programatically called.", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticTextDBInfoText.Wrap( -1 )
		gbSizerPage2.Add( self.m_staticTextDBInfoText, wx.GBPosition( 3, 1 ), wx.GBSpan( 1, 10 ), wx.ALL|wx.EXPAND, 5 )
		
		self.m_gridDB = CustomColumnGrid( self.m_panelPage2, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,400 ), wx.HSCROLL|wx.SIMPLE_BORDER|wx.VSCROLL )
		
		# Grid
		self.m_gridDB.CreateGrid( 1, 4 )
		self.m_gridDB.EnableEditing( True )
		self.m_gridDB.EnableGridLines( True )
		self.m_gridDB.EnableDragGridSize( False )
		self.m_gridDB.SetMargins( 0, 0 )
		
		# Columns
		self.m_gridDB.EnableDragColMove( False )
		self.m_gridDB.EnableDragColSize( True )
		self.m_gridDB.SetColLabelSize( 30 )
		self.m_gridDB.SetColLabelAlignment( wx.ALIGN_LEFT, wx.ALIGN_CENTRE )
		
		# Rows
		self.m_gridDB.AutoSizeRows()
		self.m_gridDB.EnableDragRowSize( True )
		self.m_gridDB.SetRowLabelSize( 80 )
		self.m_gridDB.SetRowLabelAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )
		
		# Label Appearance
		
		# Cell Defaults
		self.m_gridDB.SetDefaultCellAlignment( wx.ALIGN_LEFT, wx.ALIGN_TOP )
		gbSizerPage2.Add( self.m_gridDB, wx.GBPosition( 5, 1 ), wx.GBSpan( 5, 10 ), wx.ALL|wx.EXPAND, 0 )
		
		
		gbSizerPage2.AddGrowableCol( 10 )
		
		self.m_panelPage2.SetSizer( gbSizerPage2 )
		self.m_panelPage2.Layout()
		gbSizerPage2.Fit( self.m_panelPage2 )
		self.m_notebook.AddPage( self.m_panelPage2, u"Database Settings", False )
		self.m_panelPage3 = wx.Panel( self.m_notebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		gbSizerPage3 = wx.GridBagSizer( 0, 0 )
		gbSizerPage3.SetFlexibleDirection( wx.BOTH )
		gbSizerPage3.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_buttonImport = wx.Button( self.m_panelPage3, wx.ID_ANY, u"Start Import", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_buttonImport.SetToolTipString( u"Click to start import process." )
		
		gbSizerPage3.Add( self.m_buttonImport, wx.GBPosition( 0, 0 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_buttonImportClearGrid = wx.Button( self.m_panelPage3, wx.ID_ANY, u"Clear Grid", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_buttonImportClearGrid.SetToolTipString( u"Clears the grid below which shows to be imported data." )
		
		gbSizerPage3.Add( self.m_buttonImportClearGrid, wx.GBPosition( 0, 1 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_checkBoxStopOnError = wx.CheckBox( self.m_panelPage3, wx.ID_ANY, u"Stop On Error", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_checkBoxStopOnError.SetToolTipString( u"If enabled, any error during import process can cancel import process and prints error messages." )
		
		gbSizerPage3.Add( self.m_checkBoxStopOnError, wx.GBPosition( 0, 2 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_checkBoxDBApiVerbose = wx.CheckBox( self.m_panelPage3, wx.ID_ANY, u"Verbose Database API Ouput", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_checkBoxDBApiVerbose.SetToolTipString( u"Database operations will be logged to the output if this is enabled." )
		
		gbSizerPage3.Add( self.m_checkBoxDBApiVerbose, wx.GBPosition( 0, 3 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_staticTextImportText = wx.StaticText( self.m_panelPage3, wx.ID_ANY, u"To be imported data will be listed in the grid below firstly.", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticTextImportText.Wrap( -1 )
		gbSizerPage3.Add( self.m_staticTextImportText, wx.GBPosition( 1, 0 ), wx.GBSpan( 1, 10 ), wx.ALL|wx.EXPAND, 5 )
		
		self.m_gridImport = wx.grid.Grid( self.m_panelPage3, wx.ID_ANY, wx.DefaultPosition, wx.Size( 500,200 ), 0 )
		
		# Grid
		self.m_gridImport.CreateGrid( 5, 5 )
		self.m_gridImport.EnableEditing( False )
		self.m_gridImport.EnableGridLines( True )
		self.m_gridImport.EnableDragGridSize( False )
		self.m_gridImport.SetMargins( 0, 0 )
		
		# Columns
		self.m_gridImport.EnableDragColMove( False )
		self.m_gridImport.EnableDragColSize( True )
		self.m_gridImport.SetColLabelSize( 30 )
		self.m_gridImport.SetColLabelAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )
		
		# Rows
		self.m_gridImport.EnableDragRowSize( True )
		self.m_gridImport.SetRowLabelSize( 80 )
		self.m_gridImport.SetRowLabelAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )
		
		# Label Appearance
		
		# Cell Defaults
		self.m_gridImport.SetDefaultCellAlignment( wx.ALIGN_LEFT, wx.ALIGN_TOP )
		self.m_gridImport.SetToolTipString( u"This grid shows to be imported data in columns as understood by csv2db-importer. If error occurs, erroneous row is colored." )
		
		gbSizerPage3.Add( self.m_gridImport, wx.GBPosition( 2, 0 ), wx.GBSpan( 1, 10 ), wx.ALL|wx.EXPAND, 0 )
		
		self.m_staticTextOutput = wx.StaticText( self.m_panelPage3, wx.ID_ANY, u"Output", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticTextOutput.Wrap( -1 )
		gbSizerPage3.Add( self.m_staticTextOutput, wx.GBPosition( 3, 0 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_buttonClearImpOutput = wx.Button( self.m_panelPage3, wx.ID_ANY, u"Clear Output", wx.DefaultPosition, wx.DefaultSize, 0 )
		gbSizerPage3.Add( self.m_buttonClearImpOutput, wx.GBPosition( 3, 1 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )
		
		self.m_textCtrlImportOutput = wx.TextCtrl( self.m_panelPage3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 500,300 ), wx.HSCROLL|wx.TE_MULTILINE )
		gbSizerPage3.Add( self.m_textCtrlImportOutput, wx.GBPosition( 4, 0 ), wx.GBSpan( 2, 10 ), wx.ALL|wx.EXPAND, 5 )
		
		
		gbSizerPage3.AddGrowableCol( 9 )
		gbSizerPage3.AddGrowableRow( 2 )
		
		self.m_panelPage3.SetSizer( gbSizerPage3 )
		self.m_panelPage3.Layout()
		gbSizerPage3.Fit( self.m_panelPage3 )
		self.m_notebook.AddPage( self.m_panelPage3, u"Import", False )
		
		bSizerNotebook.Add( self.m_notebook, 1, wx.EXPAND |wx.ALL, 0 )
		
		
		self.m_panelBottom.SetSizer( bSizerNotebook )
		self.m_panelBottom.Layout()
		bSizerNotebook.Fit( self.m_panelBottom )
		bSizerScrolledWindow.Add( self.m_panelBottom, 9, wx.ALL|wx.EXPAND, 0 )
		
		
		self.m_scrolledWindowMain.SetSizer( bSizerScrolledWindow )
		self.m_scrolledWindowMain.Layout()
		bSizerScrolledWindow.Fit( self.m_scrolledWindowMain )
		bSizerFrame.Add( self.m_scrolledWindowMain, 1, wx.EXPAND, 0 )
		
		
		self.SetSizer( bSizerFrame )
		self.Layout()
		self.m_menubar = wx.MenuBar( 0 )
		self.m_menuFile = wx.Menu()
		self.m_menuItemNew = wx.MenuItem( self.m_menuFile, wx.ID_ANY, u"&New", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menuFile.AppendItem( self.m_menuItemNew )
		
		self.m_menuItemOpenFile = wx.MenuItem( self.m_menuFile, wx.ID_ANY, u"&Open File...", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menuFile.AppendItem( self.m_menuItemOpenFile )
		
		self.m_menuItemSave = wx.MenuItem( self.m_menuFile, wx.ID_ANY, u"&Save", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menuFile.AppendItem( self.m_menuItemSave )
		
		self.m_menuItemSaveAs = wx.MenuItem( self.m_menuFile, wx.ID_ANY, u"Save &As...", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menuFile.AppendItem( self.m_menuItemSaveAs )
		
		self.m_menuItemClearAll = wx.MenuItem( self.m_menuFile, wx.ID_ANY, u"&Clear", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menuFile.AppendItem( self.m_menuItemClearAll )
		
		self.m_menuItemQuit = wx.MenuItem( self.m_menuFile, wx.ID_ANY, u"&Quit", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menuFile.AppendItem( self.m_menuItemQuit )
		
		self.m_menubar.Append( self.m_menuFile, u"&File" ) 
		
		self.m_menuHelp = wx.Menu()
		self.m_menuItemHelp = wx.MenuItem( self.m_menuHelp, wx.ID_ANY, u"&Help", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menuHelp.AppendItem( self.m_menuItemHelp )
		
		self.m_menuItemAbout = wx.MenuItem( self.m_menuHelp, wx.ID_ANY, u"&About", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menuHelp.AppendItem( self.m_menuItemAbout )
		
		self.m_menubar.Append( self.m_menuHelp, u"&Help" ) 
		
		self.SetMenuBar( self.m_menubar )
		
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.m_filePickerCSV.Bind( wx.EVT_FILEPICKER_CHANGED, self.on_csv_file_selected )
		self.m_comboBoxCSVFileEnc.Bind( wx.EVT_TEXT, self.on_csv_file_encoding_edited )
		self.m_filePickerSQLitePath.Bind( wx.EVT_FILEPICKER_CHANGED, self.on_sqlite_file_selected )
		self.m_buttonGetTableDef.Bind( wx.EVT_BUTTON, self.get_table_definition )
		self.m_buttonDBGridAdd.Bind( wx.EVT_BUTTON, self.add_db_grid_row )
		self.m_buttonSortDBGrid.Bind( wx.EVT_BUTTON, self.sort_db_grid )
		self.m_buttonDBRowDel.Bind( wx.EVT_BUTTON, self.delete_db_grid_row )
		self.m_buttonImport.Bind( wx.EVT_BUTTON, self.start_import )
		self.m_buttonImportClearGrid.Bind( wx.EVT_BUTTON, self.clear_import_grid )
		self.m_checkBoxDBApiVerbose.Bind( wx.EVT_CHECKBOX, self.on_verbose_dbapi_output )
		self.m_buttonClearImpOutput.Bind( wx.EVT_BUTTON, self.clear_import_output )
		self.Bind( wx.EVT_MENU, self.new_file, id = self.m_menuItemNew.GetId() )
		self.Bind( wx.EVT_MENU, self.open_app_file, id = self.m_menuItemOpenFile.GetId() )
		self.Bind( wx.EVT_MENU, self.save_app_file, id = self.m_menuItemSave.GetId() )
		self.Bind( wx.EVT_MENU, self.saveas_app_file, id = self.m_menuItemSaveAs.GetId() )
		self.Bind( wx.EVT_MENU, self.clear_all_settings, id = self.m_menuItemClearAll.GetId() )
		self.Bind( wx.EVT_MENU, self.quit, id = self.m_menuItemQuit.GetId() )
		self.Bind( wx.EVT_MENU, self.help_menu, id = self.m_menuItemHelp.GetId() )
		self.Bind( wx.EVT_MENU, self.about_menu, id = self.m_menuItemAbout.GetId() )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def on_csv_file_selected( self, event ):
		event.Skip()
	
	def on_csv_file_encoding_edited( self, event ):
		event.Skip()
	
	def on_sqlite_file_selected( self, event ):
		event.Skip()
	
	def get_table_definition( self, event ):
		event.Skip()
	
	def add_db_grid_row( self, event ):
		event.Skip()
	
	def sort_db_grid( self, event ):
		event.Skip()
	
	def delete_db_grid_row( self, event ):
		event.Skip()
	
	def start_import( self, event ):
		event.Skip()
	
	def clear_import_grid( self, event ):
		event.Skip()
	
	def on_verbose_dbapi_output( self, event ):
		event.Skip()
	
	def clear_import_output( self, event ):
		event.Skip()
	
	def new_file( self, event ):
		event.Skip()
	
	def open_app_file( self, event ):
		event.Skip()
	
	def save_app_file( self, event ):
		event.Skip()
	
	def saveas_app_file( self, event ):
		event.Skip()
	
	def clear_all_settings( self, event ):
		event.Skip()
	
	def quit( self, event ):
		event.Skip()
	
	def help_menu( self, event ):
		event.Skip()
	
	def about_menu( self, event ):
		event.Skip()
	

