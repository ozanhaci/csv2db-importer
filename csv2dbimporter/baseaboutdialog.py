# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jun 17 2015)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class BaseAboutDialog
###########################################################################

class BaseAboutDialog ( wx.Dialog ):
	
	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"About", pos = wx.DefaultPosition, size = wx.DefaultSize, style = wx.DEFAULT_DIALOG_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizerParent = wx.BoxSizer( wx.VERTICAL )
		
		self.m_staticTextProgName = wx.StaticText( self, wx.ID_ANY, u"csv2db-importer", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.m_staticTextProgName.Wrap( -1 )
		self.m_staticTextProgName.SetFont( wx.Font( 20, 70, 90, 92, False, wx.EmptyString ) )
		
		bSizerParent.Add( self.m_staticTextProgName, 0, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		self.m_staticTextVersion = wx.StaticText( self, wx.ID_ANY, u"Version", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticTextVersion.Wrap( -1 )
		self.m_staticTextVersion.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		bSizerParent.Add( self.m_staticTextVersion, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		self.m_staticTextCopyRight = wx.StaticText( self, wx.ID_ANY, u"© 2016 Ozan HACIBEKİROĞLU (ozan_haci@yahoo.com)", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.m_staticTextCopyRight.Wrap( -1 )
		self.m_staticTextCopyRight.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 90, False, wx.EmptyString ) )
		
		bSizerParent.Add( self.m_staticTextCopyRight, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		self.m_staticTextLicense = wx.StaticText( self, wx.ID_ANY, u"License: MIT", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticTextLicense.Wrap( -1 )
		bSizerParent.Add( self.m_staticTextLicense, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		m_sdbSizer = wx.StdDialogButtonSizer()
		self.m_sdbSizerOK = wx.Button( self, wx.ID_OK )
		m_sdbSizer.AddButton( self.m_sdbSizerOK )
		m_sdbSizer.Realize();
		
		bSizerParent.Add( m_sdbSizer, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		self.SetSizer( bSizerParent )
		self.Layout()
		bSizerParent.Fit( self )
		
		self.Centre( wx.BOTH )
	
	def __del__( self ):
		pass
	

