# encoding: utf-8
from __future__ import unicode_literals, print_function, absolute_import
import logging
from operator import itemgetter

import wx.grid as gridlib

from .core import column_types_dict, DevNull
from .baseaboutdialog import BaseAboutDialog


class TextCtrlLogHandler(logging.StreamHandler):
    def __init__(self, stream=None, text_ctrl=None):
        if stream is None:
            stream = DevNull()
        assert text_ctrl is not None, "wx.TextCtrl object  is required"
        assert hasattr(text_ctrl, "AppendText"), "TextCtrl object must have AppendText method"
        logging.StreamHandler.__init__(self, stream=stream)
        self.text_ctrl = text_ctrl

    def emit(self, record):
        logging.StreamHandler.emit(self, record)
        msg = self.format(record)
        ufs = u'%s\n'
        self.text_ctrl.AppendText(ufs % msg)


class CustomColumnDataTable(gridlib.PyGridTableBase):
    def __init__(self):
        gridlib.PyGridTableBase.__init__(self)
        self.colLabels = ['Order', 'Column Name', 'Column Type', 'Primary Key', 'Default']

        self.dataTypes = [gridlib.GRID_VALUE_NUMBER,
                          gridlib.GRID_VALUE_STRING,
                          gridlib.GRID_VALUE_CHOICE + ':,' + ",".join(list(column_types_dict.keys())),
                          gridlib.GRID_VALUE_BOOL,
                          gridlib.GRID_VALUE_STRING,
                          ]

        self.data = [[0, "", "", False, ""], ]

    def SetValue(self, row, col, val):
        """SetValue(self, int row, int col, String value)"""
        lendata = self.GetNumberRows()
        if lendata == 0:
            self.AppendRows(numRows=1)
            return self.SetValue(row, col, val)
        elif row + 1 > lendata:
            self.AppendRows(numRows=(row - lendata + 1))
            return self.SetValue(row, col, val)
        else:
            self.data[row][col] = val
            msg = gridlib.GridTableMessage(self, gridlib.GRIDTABLE_REQUEST_VIEW_GET_VALUES)
            self.GetView().ProcessTableMessage(msg)

    def AppendRows(self, numRows=1):
        self.GetView().DisableCellEditControl()
        self.data.append([0, "", "", False, ""] * numRows)
        msg = gridlib.GridTableMessage(self, gridlib.GRIDTABLE_NOTIFY_ROWS_APPENDED, numRows)
        self.GetView().ProcessTableMessage(msg)
        return True

    def DeleteRows(self, pos=0, numRows=1):
        self.GetView().DisableCellEditControl()
        del self.data[pos:pos + numRows]
        msg = gridlib.GridTableMessage(self, gridlib.GRIDTABLE_NOTIFY_ROWS_DELETED, pos, numRows)
        self.GetView().ProcessTableMessage(msg)
        return True

    def SortRows(self, colno=0):
        self.GetView().DisableCellEditControl()
        self.data = sorted(self.data, key=itemgetter(colno))
        msg = gridlib.GridTableMessage(self, gridlib.GRIDTABLE_REQUEST_VIEW_GET_VALUES)
        self.GetView().ProcessTableMessage(msg)

    def GetNumberRows(self):
        return len(self.data)

    def GetNumberCols(self):
        try:
            return len(self.data[0])
        except IndexError:
            return 0

    def IsEmptyCell(self, row, col):
        try:
            return not self.data[row][col]
        except IndexError:
            return True

    # Get/Set values in the table.  The Python version of these
    # methods can handle any data-type, (as long as the Editor and
    # Renderer understands the type too,) not just strings as in the
    # C++ version.
    def GetValue(self, row, col):
        try:
            return self.data[row][col]
        except IndexError:
            return ''

    # Called when the grid needs to display labels
    def GetColLabelValue(self, col):
        return self.colLabels[col]

    # Called to determine the kind of editor/renderer to use by
    # default, doesn't necessarily have to be the same type used
    # natively by the editor/renderer if they know how to convert.
    def GetTypeName(self, row, col):
        return self.dataTypes[col]

    # Called to determine how the data can be fetched and stored by the
    # editor and renderer.  This allows you to enforce some type-safety
    # in the grid.
    def CanGetValueAs(self, row, col, typeName):
        colType = self.dataTypes[col].split(':')[0]
        if typeName == colType:
            return True
        else:
            return False

    def CanSetValueAs(self, row, col, typeName):
        return self.CanGetValueAs(row, col, typeName)


class CustomColumnGrid(gridlib.Grid):
    def __init__(self, parent, ID, position, size, style):
        gridlib.Grid.__init__(self, parent, ID, position, size, style)

        pygrid_table = CustomColumnDataTable()

        # The second parameter means that the grid is to take ownership of the
        # table and will destroy it when done.  Otherwise you would need to keep
        # a reference to it and call it's Destroy method later.
        self.SetTable(pygrid_table, True)

        self.SetRowLabelSize(0)
        self.SetMargins(0, 0)
        self.AutoSizeColumns(False)

        self.Bind(gridlib.EVT_GRID_CELL_LEFT_DCLICK, self.OnLeftDClick)

    def ClearGrid(self):
        table = self.GetTable()
        numrows = table.GetNumberRows()
        table.DeleteRows(pos=0, numRows=numrows)

    def CreateGrid(self, *args, **kw):
        pass

    def SortRows(self, colno=0):
        return self.GetTable().SortRows(colno=colno)

    def SetValue(self, row, col, val):
        return self.GetTable().SetValue(row, col, val)

    def GetTableData(self):
        return self.GetTable().data

    def SetTableData(self, data):
        self.GetTable().data = data

    # In addition to starting cell editor on second click,
    # double click starts as well
    def OnLeftDClick(self, evt):
        if self.CanEnableCellControl():
            self.EnableCellEditControl()


class AboutDialog(BaseAboutDialog):
    def __init__(self, parent):
        super(AboutDialog, self).__init__(parent)
        version = __import__("__main__").__version__
        self.m_staticTextVersion.SetLabel("Version " + str(version))
