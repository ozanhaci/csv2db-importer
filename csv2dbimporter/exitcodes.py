# encoding: utf-8

EXIT_NORMAL = 0
EXIT_ERROR = 1
EXIT_INVALID_FILE_ERROR = 10
EXIT_GUI_REQUIRED_ERROR = 11
EXIT_UNEXPECTED_ERROR = 100
EXIT_UNIDENTIFIED_ERROR = 101
