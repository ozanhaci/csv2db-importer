# encoding: utf-8
from __future__ import unicode_literals, absolute_import, print_function
import os
import sys
import traceback
import logging
import webbrowser
import urllib

import wx.grid

from .basemainframe import BaseMainFrame
from .db import DBApi, DBImport
from .mylib import TextCtrlLogHandler, AboutDialog
from .core import (check_csv2db_file_valid, CsvSettings, DBSettings,
                   UserSettings, save_settings, CsvImport, CsvReadError,
                   detect_file_encoding, DevNull)


class MainFrame(BaseMainFrame):

    def __init__(self):
        super(MainFrame, self).__init__(parent=None)
        self.current_open_file = ""
        self.current_settings = None

        # create a handler for sqlalchemy logs which are append to textctrl object given
        self.dbapi_log_handler = TextCtrlLogHandler(text_ctrl=self.m_textCtrlImportOutput)
        logging.basicConfig(stream=DevNull())
        logging.getLogger('sqlalchemy').addHandler(self.dbapi_log_handler)
        logging.getLogger('sqlalchemy').setLevel(logging.INFO)

        self.Show()

    def help_menu(self, event):
        event.Skip()
        version = __import__("__main__").__version__
        version = urllib.quote(version)
        webbrowser.open("http://ozanh.com/csv2db-importer/help.php?from=" + version)

    def about_menu(self, event):
        event.Skip()
        dlg = AboutDialog(parent=self)
        dlg.ShowModal()
        dlg.Destroy()

    def on_verbose_dbapi_output(self, event=None):
        obj = event.GetEventObject()
        if not obj.IsChecked():
            logging.getLogger('sqlalchemy').removeHandler(self.dbapi_log_handler)
        else:
            logging.getLogger('sqlalchemy').removeHandler(self.dbapi_log_handler)
            logging.getLogger('sqlalchemy').addHandler(self.dbapi_log_handler)

    def on_csv_file_encoding_edited(self, event):
        event.Skip()
        self.m_checkBoxCSVEncAutoDetected.SetValue(False)

    def on_csv_file_selected(self, event=None):
        '''
        Guess the file's encoding after selected
        '''
        if event:
            event.Skip()
        path = self.m_filePickerCSV.GetPath()
        if not path:
            return
        result = detect_file_encoding(path)
        if result and "encoding" in result and "confidence" in result:
            with wx.BusyCursor():
                self.m_comboBoxCSVFileEnc.SetValue(result["encoding"])
                self.m_checkBoxCSVEncAutoDetected.SetValue(True)
                if not event:
                    return

            conf = result["confidence"]
            if conf > 0:
                conf *= 100
                conf = round(conf, 1)
                conf = str(conf) + "%"
                self.m_infoCtrlMain.ShowMessage("%s sure that CSV file encoding is %s" % (conf,
                                                                                          result["encoding"]))
                wx.CallLater(5000, self.m_infoCtrlMain.Dismiss)
                wx.CallLater(6000, self.Refresh)

    def on_sqlite_file_selected(self, event):
        '''
        After sqlite database file selected, create the db url and put it
        in textctrl 
        '''
        event.Skip()
        obj = event.GetEventObject()
        path = obj.GetPath()
        if path:
            self.m_textCtrlDbConUrl.SetValue("sqlite:///" + path)
            if not os.path.exists(path):
                self.m_infoCtrlMain.ShowMessage("Selected file does not exists",
                                                wx.ICON_ERROR)
                wx.CallLater(5000, self.m_infoCtrlMain.Dismiss)

    @property
    def current_open_file(self):
        return self._current_open_file

    @current_open_file.setter
    def current_open_file(self, v):
        title = self.GetTitle()
        name = title.split(' ', 1)[0]
        _v = v
        if v:
            _v = '"%s"' % v
            self.m_menuItemSave.Enable(True)
            self.m_menuItemSaveAs.Enable(True)
            self.SetTitle("%s %s" % (name, _v))
        else:
            self.m_menuItemSave.Enable(False)
            self.m_menuItemSaveAs.Enable(False)
            self.SetTitle(name)
        self._current_open_file = v

    def add_db_grid_row(self, event):
        self.m_gridDB.AppendRows()
        event.Skip()

    def sort_db_grid(self, event):
        self.m_gridDB.SortRows()
        event.Skip()

    def get_table_definition(self, event):
        event.Skip()
        url = self.m_textCtrlDbConUrl.GetValue()
        table_name = self.m_comboBoxTableName.GetValue() or None
        self.dbapi = DBApi(url=url, table_name=table_name,
                           engine_kw={"echo": False})
        if table_name is None:
            tables = self.dbapi.inspector.get_table_names()
            self.m_comboBoxTableName.AppendItems(tables)
            self.m_comboBoxTableName.SetFocus()
            self.m_infoCtrlMain.ShowMessage("Tables are fetched and appended")
            wx.CallLater(2000, self.m_infoCtrlMain.Dismiss)
            self.m_comboBoxTableName.Popup()
            self.dbapi.close()
            return
        dlg = wx.MessageDialog(self, "Do you want to copy column info to the grid?",
                               "Question", wx.ICON_INFORMATION | wx.YES_NO)
        if dlg.ShowModal() == wx.ID_YES:
            self.m_gridDB.ClearGrid()
            columns = self.dbapi.inspector.get_columns()
            for idx, c in enumerate(columns):
                default = c["default"] or ""
                # tiny hack for misinterpretation of inspection module of sqlalchemy
                if default.upper() == "NULL":
                    default = ""
                self.m_gridDB.SetValue(idx, 0, idx + 1)
                self.m_gridDB.SetValue(idx, 1, c["name"])
                self.m_gridDB.SetValue(idx, 2, "")
                self.m_gridDB.SetValue(idx, 3, c["primary_key"])
                self.m_gridDB.SetValue(idx, 4, default)
        self.dbapi.close()
        dlg.Destroy()

    def open_app_file(self, event):
        event.Skip()
        error_msg_invalid_file = "File is not a valid csv2db-importer file"
        dlg = wx.FileDialog(self, defaultDir=os.getcwd(),
                            style=wx.OPEN | wx.CHANGE_DIR,
                            message="Select csv2db File", wildcard="*.csv2db")
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            f, ext = os.path.splitext(path)
            if ext.lower() != ".csv2db":
                self.m_infoCtrlMain.ShowMessage(error_msg_invalid_file, wx.ICON_ERROR)
                wx.CallLater(2000, self.m_infoCtrlMain.Dismiss)
                dlg.Destroy()
                return
            ret, msg, settings = check_csv2db_file_valid(path=path)
            if not ret:
                wx.MessageBox(error_msg_invalid_file + "\n" + msg, "Error",
                              wx.OK | wx.ICON_ERROR)
                dlg.Destroy()
                return
            self.current_open_file = path
            self.current_settings = settings
            self.clear_inputs()
            self.set_inputs()
        dlg.Destroy()

    def clear_all_settings(self, event=None):
        if event:
            event.Skip()
        self.clear_inputs()
        self.current_open_file = ""
        self.current_settings = None

    def clear_inputs(self):
        self.m_comboBoxColSep.SetValue("")
        self.m_comboBoxDecPoint.SetValue("")
        self.m_comboBoxStrDel.SetValue("")
        self.m_comboBoxThousandSep.SetValue("")
        self.m_comboBoxDTSep.SetValue("")
        self.m_filePickerCSV.SetPath("")
        self.m_comboBoxCSVFileEnc.SetValue("")
        self.m_textCtrlDateFormat.Clear()
        self.m_textCtrlDbConUrl.Clear()
        self.m_textCtrlImportOutput.Clear()
        self.m_comboBoxTableName.SetValue("")
        self.m_textCtrlTimeFormat.Clear()
        self.m_checkBoxInsert.SetValue(False)
        self.m_checkBoxSkipFirstRow.SetValue(False)
        self.m_checkBoxCSVEncAutoDetected.SetValue(False)
        self.m_checkBoxForceCSVEncDetect.SetValue(False)
        self.m_checkBoxStopOnError.SetValue(False)
        self.m_checkBoxDBApiVerbose.SetValue(False)
        self.m_checkBoxUpdate.SetValue(False)
        self.m_gridDB.ClearGrid()
        self.m_gridImport.ClearGrid()

    def set_inputs(self):
        # csv
        val = self.current_settings.csv.column_separator or ""
        self.m_comboBoxColSep.SetValue(val)
        val = self.current_settings.csv.decimal_point or ""
        self.m_comboBoxDecPoint.SetValue(val)
        val = self.current_settings.csv.string_delimiter or ""
        self.m_comboBoxStrDel.SetValue(val)
        val = self.current_settings.csv.thousand_separator or ""
        self.m_comboBoxThousandSep.SetValue(val)
        val = self.current_settings.csv.dt_sep or ""
        self.m_comboBoxDTSep.SetValue(val)
        val = self.current_settings.csv.file_path or ""
        self.m_filePickerCSV.SetPath(val)
        val = self.current_settings.csv.file_encoding or ""
        self.m_comboBoxCSVFileEnc.SetValue(val)
        val = self.current_settings.csv.date_format or ""
        self.m_textCtrlDateFormat.SetValue(val)
        val = self.current_settings.csv.time_format or ""
        self.m_textCtrlTimeFormat.SetValue(val)
        val = self.current_settings.csv.skip_first_row
        self.m_checkBoxSkipFirstRow.SetValue(bool(val))
        val = self.current_settings.csv.encoding_auto_detected
        self.m_checkBoxCSVEncAutoDetected.SetValue(bool(val))
        val = self.current_settings.csv.force_encoding_detection
        self.m_checkBoxForceCSVEncDetect.SetValue(bool(val))
        # db
        val = self.current_settings.db.url or ""
        self.m_textCtrlDbConUrl.SetValue(val)
        val = self.current_settings.db.table_name or ""
        self.m_comboBoxTableName.SetValue(val)
        val = self.current_settings.db.insert
        self.m_checkBoxInsert.SetValue(bool(val))
        val = self.current_settings.db.update
        self.m_checkBoxUpdate.SetValue(bool(val))
        columns = self.current_settings.db.columns.val
        nrows = self.m_gridDB.GetNumberRows()
        if len(columns) > nrows:
            self.m_gridDB.AppendRows(len(columns) - nrows)
        if columns:
            self.m_gridDB.SetTableData([])
        data = self.m_gridDB.GetTableData()
        for c in columns:
            data.append(c.values())
        # general
        val = self.current_settings.stop_on_error
        self.m_checkBoxStopOnError.SetValue(bool(val))
        val = self.current_settings.dbapi_verbose
        self.m_checkBoxDBApiVerbose.SetValue(bool(val))

        self.m_gridDB.SortRows()

    def build_settings(self):
        vdict = {}
        vdict["date_format"] = self.m_textCtrlDateFormat.GetValue()
        vdict["time_format"] = self.m_textCtrlTimeFormat.GetValue()
        vdict["dt_sep"] = self.m_comboBoxDTSep.GetValue()
        vdict["column_separator"] = self.m_comboBoxColSep.GetValue()
        vdict["string_delimiter"] = self.m_comboBoxStrDel.GetValue()
        vdict["decimal_point"] = self.m_comboBoxDecPoint.GetValue()
        vdict["thousand_separator"] = self.m_comboBoxThousandSep.GetValue()
        vdict["skip_first_row"] = self.m_checkBoxSkipFirstRow.GetValue()
        vdict["file_path"] = self.m_filePickerCSV.GetPath()
        force_detection = self.m_checkBoxForceCSVEncDetect.GetValue()
        vdict["force_encoding_detection"] = force_detection
        is_auto_enc = self.m_checkBoxCSVEncAutoDetected.GetValue()
        vdict["encoding_auto_detected"] = is_auto_enc
        if not is_auto_enc and force_detection:
            self.on_csv_file_selected()
        vdict["file_encoding"] = self.m_comboBoxCSVFileEnc.GetValue()
        csv_settings = CsvSettings(**vdict)

        vdict = {}
        vdict["url"] = self.m_textCtrlDbConUrl.GetValue()
        vdict["table_name"] = self.m_comboBoxTableName.GetValue()
        vdict["insert"] = self.m_checkBoxInsert.GetValue()
        vdict["update"] = self.m_checkBoxUpdate.GetValue()
        db_settings = DBSettings(**vdict)

        grid_data = self.m_gridDB.GetTableData()[:]
        for row in grid_data:
            db_settings.columns.add(*row)

        stop_on_error = self.m_checkBoxStopOnError.GetValue()
        dbapi_verbose = self.m_checkBoxDBApiVerbose.GetValue()

        user_settings = UserSettings(csv=csv_settings, db=db_settings,
                                     stop_on_error=stop_on_error,
                                     dbapi_verbose=dbapi_verbose)
        return user_settings

    def start_import(self, event):
        if event:
            event.Skip()
        # sort the columns in db grid
        self.m_gridDB.SortRows()
        # clear the grid and output textctrl
        self.m_textCtrlImportOutput.Clear()
        self.m_gridImport.ClearGrid()
        # get the current settings
        settings = self.build_settings()
        # remove the handler of dbapi from logger if unchecked
        if not settings.dbapi_verbose:
            logging.getLogger("sqlalchemy").removeHandler(self.dbapi_log_handler)
        # create the wrapper instance of csv reader
        csvimp = CsvImport(settings=settings)

        lenrec = len(settings.db.columns.val)
        ncols = self.m_gridImport.GetNumberCols()
        if lenrec > ncols:
            self.m_gridImport.AppendCols(lenrec - ncols)
        elif lenrec < ncols:
            self.m_gridImport.DeleteCols(ncols - lenrec)
        for cidx, cname in enumerate(settings.db.columns.val):
            self.m_gridImport.SetColLabelValue(cidx, cname.column_name)

        cur_row_no = 0
        has_error = row_error = False
        for rec in csvimp.read_csv_line():
            if isinstance(rec, CsvReadError):
                try:
                    raise rec.exc_cls, rec.exc, rec.tb
                except Exception as e:
                    exc = traceback.format_exc() + "\n"
                    if isinstance(e, IndexError):
                        exc += "Please check CSV file columns count and DB Settings columns count are equal\n"
                    sys.stderr.write(exc)
                    self.m_textCtrlImportOutput.AppendText(exc)
                    self.m_infoCtrlMain.ShowMessage(unicode(e), wx.ICON_ERROR)
                    attr = wx.grid.GridCellAttr()
                    attr.SetTextColour(wx.BLACK)
                    attr.SetBackgroundColour(wx.RED)
                    self.m_gridImport.SetRowAttr(cur_row_no, attr)
                    self.m_gridImport.SetCellValue(cur_row_no, 0, "ERROR")
                    has_error = row_error = True
                    if not settings.stop_on_error:
                        continue
                    else:
                        break
            for cidx, cval in enumerate(rec):
                if cval is None:
                    cval = ""
                self.m_gridImport.SetCellValue(cur_row_no, cidx, "%s" % cval)
            if not row_error:
                self.m_gridImport.SetRowAttr(cur_row_no, wx.grid.GridCellAttr())
            else:
                row_error = False
            cur_row_no += 1
            num_cur_row = self.m_gridImport.GetNumberRows()
            if cur_row_no + 1 > num_cur_row:
                self.m_gridImport.AppendRows(cur_row_no + 1 - num_cur_row + 10)
        self.m_gridImport.Refresh()
        if has_error:
            self.m_infoCtrlMain.ShowMessage("Import has errors, fix and try it again",
                                            wx.ICON_ERROR)
            self.m_textCtrlImportOutput.AppendText("Importing to database is cancelled due to errors")
            self.Layout()
            self.Refresh()
            wx.CallLater(5000, self.m_infoCtrlMain.Dismiss)
            wx.CallLater(6000, self.Refresh)
        else:
            dlg = wx.MessageDialog(self, "Do you want to continue to import records to Database",
                                 "Continue?", wx.YES_NO | wx.YES_DEFAULT | wx.ICON_INFORMATION)
            if dlg.ShowModal() != wx.ID_YES:
                dlg.Destroy()
                return
            dlg.Destroy()
            url = self.m_textCtrlDbConUrl.GetValue()
            table_name = self.m_comboBoxTableName.GetValue()
            dbapi = DBApi(url=url, table_name=table_name, engine_kw={"echo": False})
            dbapi.create_engine()
            settings = self.build_settings()
            dbimport = DBImport(dbapi=dbapi, settings=settings)
            columns = settings.db.columns.val
            insert_ok = settings.db.insert
            update_ok = settings.db.update
            stop_on_error = settings.stop_on_error
            dbimport.start_transaction()
            for lineno, rec in enumerate(csvimp.read_csv_line()):
                insert_error = update_error = False
                tb_str = ""
                rec_dict = {}
                for idx, r in enumerate(rec):
                    rec_dict[columns[idx].column_name] = r
                if insert_ok:
                    try:
                        res = dbimport.add_record(rec=rec_dict)
                    except:
                        tb_str = traceback.format_exc()
                        insert_error = True
                if(insert_error and update_ok) or (not insert_error and not insert_ok and update_ok):
                    try:
                        res = dbimport.update_record(rec=rec_dict)
                        if insert_error:
                            insert_error = False
                    except:
                        tb_str = traceback.format_exc()
                        update_error = True

                if insert_error or update_error:
                    tb_str += "\n"
                    sys.stderr.write(tb_str)
                    type_ = "Updating" if update_error else "Adding"
                    errstr = "ERROR While %s record=%r" % (type_, rec_dict) + "\n"
                    errstr += "Line No=%s" % (lineno + 1) + "\n"
                    sys.stderr.write(errstr)
                    self.m_textCtrlImportOutput.AppendText(tb_str)
                    self.m_textCtrlImportOutput.AppendText(errstr)
                    if stop_on_error:
                        break
            if stop_on_error and (update_error or insert_error):
                dbimport.rollback()
            else:
                try:
                    dbimport.commit()
                except:
                    tb_str = traceback.format_exc() + "\n"
                    errstr = "Error while commiting to database\n"
                    sys.stderr.write(tb_str)
                    sys.stderr.write(errstr)
                    self.m_textCtrlImportOutput.AppendText(tb_str)
                    self.m_textCtrlImportOutput.AppendText(errstr)
                    dbimport.rollback()

    def new_file(self, event):
        event.Skip()
        if self.current_open_file:
            dlg = wx.MessageDialog(self, "Do you want to save current open file before creating new file",
                                   "Save?", wx.YES_NO | wx.YES_DEFAULT | wx.ICON_INFORMATION)
            if dlg.ShowModal() != wx.ID_YES:
                self.save_app_file()
            dlg.Destroy()

        dlg = wx.FileDialog(self, defaultDir=os.getcwd(),
                            style=wx.OPEN | wx.CHANGE_DIR,
                            message="Create new", wildcard="*.csv2db")
        if dlg.ShowModal() == wx.ID_OK:
            self.clear_all_settings()
            path = dlg.GetPath()
            self.current_open_file = path
            self.save_app_file()
        dlg.Destroy()

    def save_app_file(self, event=None, saveas=False):
        if event:
            event.Skip()
        self.m_gridDB.DisableCellEditControl()
        self.m_gridDB.SortRows()
        if self.current_open_file and saveas is False:
            settings = self.build_settings()
            try:
                save_settings(self.current_open_file, settings)
            except Exception as e:
                wx.MessageBox(unicode(e), "Error", wx.OK | wx.ICON_ERROR)
                return
            self.m_infoCtrlMain.ShowMessage("Saved", wx.ICON_INFORMATION)
            wx.CallLater(2000, self.m_infoCtrlMain.Dismiss)
        else:
            dlg = wx.FileDialog(self, defaultDir=os.getcwd(),
                                style=wx.OPEN | wx.CHANGE_DIR,
                                message="Save As...", wildcard="*.csv2db")
            if dlg.ShowModal() == wx.ID_OK:
                path = dlg.GetPath()
                self.current_open_file = path
                self.current_settings = self.build_settings()
                dlg.Destroy()
                return self.save_app_file()
            dlg.Destroy()

    def saveas_app_file(self, event):
        return self.save_app_file(event, saveas=True)

    def delete_db_grid_row(self, event):
        while True:
            rows = self.m_gridDB.GetSelectedRows()
            if rows:
                self.m_gridDB.DeleteRows(pos=rows[0])
            else:
                break

    def clear_import_output(self, event):
        event.Skip()
        self.m_textCtrlImportOutput.Clear()

    def clear_import_grid(self, event):
        self.m_gridImport.ClearGrid()
        event.Skip()

    def quit(self, event):
        style = 0
        if self.current_open_file:
            style = wx.YES_DEFAULT
        else:
            style = wx.NO_DEFAULT
        dlg = wx.MessageDialog(self, "Do you want to save before exit?",
                               "Save ?", wx.ICON_INFORMATION | wx.YES_NO | style)
        if dlg.ShowModal() == wx.ID_YES:
            self.save_app_file(event)
        dlg.Destroy()
        self.Close()

