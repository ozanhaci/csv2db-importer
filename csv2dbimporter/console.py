# encoding: utf-8
from __future__ import unicode_literals, absolute_import, print_function
import os
import logging
from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter


from .core import check_csv2db_file_valid, CsvImport, DevNull
from .db import DBApi, DBImport
from .exitcodes import (EXIT_NORMAL, EXIT_ERROR, EXIT_INVALID_FILE_ERROR,
                        EXIT_UNIDENTIFIED_ERROR, EXIT_UNEXPECTED_ERROR,
                        EXIT_GUI_REQUIRED_ERROR)


class MainConsole(object):
    def __init__(self, argv):
        self.argv = argv

    def run(self):
        mainmod = __import__('__main__')
        program_name = "csv2db-importer"
        program_version = "v%s" % mainmod.__version__
        program_version_message = '%s (%s)' % (program_name, program_version)
        program_shortdesc = mainmod.__doc__.split("\n")[1]
        program_license = '''%s
License:The MIT License

USAGE''' % (program_shortdesc,)
        parser = ArgumentParser(description=program_license,
                                formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument('-c', '--console', dest='console', required=True,
                            action='store_true', help='to use console application')
        parser.add_argument('-V', '--version', action='version',
                            version=program_version_message)
        parser.add_argument('-f', '--file', dest="file", required=True,
                            metavar="FILEPATH",
                            help='csv2db-importer program file path')
        parser.add_argument('-L', '--log-file', default=None,
                            help='If log file is set, output will be redirected to log file instead of stderr')
        args = parser.parse_args(self.argv)
        isconsole = args.console
        if not isconsole:
            return EXIT_GUI_REQUIRED_ERROR
        try:
            return self.process(args)
        except:
            self.logger.exception("internal error")
            return EXIT_UNEXPECTED_ERROR

    def process(self, arguments):

        def return_ok():
            logger.debug("csv2db-importer finished")
            return EXIT_NORMAL

        def return_error():
            logger.debug("csv2db-importer has encountered errors")
            return EXIT_ERROR

        logging.basicConfig(stream=DevNull())
        self.logger = logger = logging.getLogger("csv2db")
        logger.setLevel(logging.DEBUG)
        log_formatter = logging.Formatter('%(asctime)s - %(name)-15s - %(levelname)-10s - %(message)s')
        if arguments.log_file:
            self.log_handler = handler = logging.FileHandler(filename=arguments.log_file, mode="a+")
        else:
            self.log_handler = handler = logging.StreamHandler()
        handler.setFormatter(log_formatter)
        logger.addHandler(handler)

        logger.debug("csvdb-importer logging is started")

        logger.debug("checking file validity")
        ret = self.check_csv2db_file(filepath=arguments.file)
        if ret != 0:
            return ret

        # import starts
        settings = self.settings
        if settings.dbapi_verbose:
            sa_logger = logging.getLogger("sqlalchemy")
            sa_logger.addHandler(self.log_handler)
            sa_logger.setLevel(logging.INFO)
        else:
            logger.debug("dbapi_verbose is False")

        csvimp = CsvImport(settings=settings)
        url = settings.db.url
        table_name = settings.db.table_name
        dbapi = DBApi(url=url, table_name=table_name, engine_kw={"echo": False})
        dbapi.create_engine()

        dbimport = DBImport(dbapi=dbapi, settings=settings)
        columns = settings.db.columns.val
        insert_ok = settings.db.insert
        update_ok = settings.db.update
        stop_on_error = settings.stop_on_error
        dbimport.start_transaction()
        logger.info("Transaction is started")
        counter = 0
        for lineno, rec in enumerate(csvimp.read_csv_line()):
            insert_error = update_error = False
            rec_dict = {}
            for idx, r in enumerate(rec):
                try:
                    rec_dict[columns[idx].column_name] = r
                except IndexError:
                    logger.exception("CSV file columns count and DB Settings columns count are equal")
                    # force to rollback after loop
                    update_error = insert_error = stop_on_error = True
                    break
            if insert_ok:
                try:
                    res = dbimport.add_record(rec=rec_dict)
                    if res:
                        counter += 1
                except:
                    if not update_ok:
                        logger.exception("insert error")
                    insert_error = True
            if(insert_error and update_ok) or (not insert_error and not insert_ok and update_ok):
                try:
                    res = dbimport.update_record(rec=rec_dict)
                    if res:
                        counter += 1
                    if insert_error:
                        insert_error = False
                except:
                    logger.exception("update error")
                    update_error = True

            if insert_error or update_error:
                type_ = "Updating" if update_error else "Adding"
                errstr = "ERROR While %s record=%r" % (type_, rec_dict) + "\n"
                errstr += "Line No=%s" % (lineno + 1) + "\n"
                logger.error(errstr)
                if stop_on_error:
                    break
        if stop_on_error and (update_error or insert_error):
            dbimport.rollback()
            return return_error()
        else:
            try:
                dbimport.commit()
                logger.info("Number of records Added/Updated=%s" % counter)
            except:
                logger.exception("database commit error")
                dbimport.rollback()
                return return_error()
        logger.info("Transaction is finished")
        # import finished
        return return_ok()

    def check_csv2db_file(self, filepath):
        if not os.path.exists(filepath):
            self.logger.error("'%s' file does not exist", filepath)
            return EXIT_INVALID_FILE_ERROR
        ret, msg, settings = check_csv2db_file_valid(path=filepath)
        if not ret:
            self.logger.error(msg)
            return EXIT_INVALID_FILE_ERROR
        self.settings = settings
        return EXIT_NORMAL
