# encoding: utf-8
from __future__ import unicode_literals
import sys
import traceback
import pickle
import copy
import csv
import datetime
import warnings
import chardet
import logging


logger = logging.getLogger("csv2db.core")


class DevNull(object):
    '''
    create file like objects from this class which does nothing
    with given argumets
    '''
    def write(self, *args):
        pass
    def flush(self, *args):
        pass
    def close(self, *args):
        pass


def conv_text(instr, setting=None):
    if not isinstance(instr, unicode):
        return unicode(instr, setting.file_encoding or "utf-8")
    return instr


def conv_int(instr, setting):
    if not instr:
        return None
    thousand_sep = setting.thousand_separator
    val = instr.replace(thousand_sep, "")
    return int(val)


def conv_float(instr, setting):
    if not instr:
        return None
    decimal_point = setting.decimal_point
    thousand_sep = setting.thousand_separator
    val = instr.replace(thousand_sep, "")
    val = val.replace(decimal_point, ".")
    return float(val)


def conv_bool(instr, setting=None):
    return bool(instr)


def conv_date(instr, setting):
    if not instr:
        return None
    return datetime.datetime.strptime(instr, setting.date_format).date()


def conv_datetime(instr, setting):
    if not instr:
        return None
    return datetime.datetime.strptime(instr, setting.date_format + setting.dt_sep + setting.time_format)


char_mapping = {"<comma>": ",", "<semicolon>": ";", "<tab>": "\t",
                "<space>": " ", "pipe": "|", "<single quotation>": "'",
                "<double quotation>": '"'}


column_types_dict = {"TEXT": conv_text, "INTEGER": conv_int, "FLOAT": conv_float,
                     "DATE": conv_date, "DATETIME": conv_datetime, "BOOL": conv_bool}


class BaseCompatSetting(object):

    def __getattr__(self, key):
        if key.startswith("_"):
            raise AttributeError("Attribute '%s' not found" % key)
        else:
            warnings.warn("Setting attribute '%s' is missing" % key)
            setattr(self, key, None)
            return getattr(self, key)


class CsvSettings(BaseCompatSetting):

    def __init__(self,
                 date_format=None,
                 time_format=None,
                 dt_sep=None,
                 column_separator=None,
                 string_delimiter=None,
                 decimal_point=None,
                 thousand_separator=None,
                 skip_first_row=False,
                 file_path=None,
                 file_encoding="utf-8",
                 force_encoding_detection=False,
                 encoding_auto_detected=False):
        self.date_format = date_format
        self.time_format = time_format
        self.dt_sep = dt_sep
        self.column_separator = column_separator
        self.string_delimiter = string_delimiter
        self.decimal_point = decimal_point
        self.thousand_separator = thousand_separator
        self.skip_first_row = skip_first_row
        self.file_path = file_path
        self.file_encoding = file_encoding
        self.force_encoding_detection = force_encoding_detection
        self.encoding_auto_detected = encoding_auto_detected


class ColumnSetting(BaseCompatSetting):
    def __init__(self,
                 order,
                 column_name=None,
                 column_type=None,
                 primary_key=False,
                 default=None):
        self.order = order
        self.column_name = column_name
        self.column_type = column_type
        self.primary_key = primary_key
        self.default = default

    def values(self):
        return [self.order, self.column_name, self.column_type, self.primary_key, self.default]


class ColumnSettings(BaseCompatSetting):
    def __init__(self):
        self.val = []

    def add(self, *args):
        '''
        arguments are order,column_name,column_type,primary key,default
        '''
        c = ColumnSetting(*args)
        self.val.append(c)


class DBSettings(BaseCompatSetting):
    def __init__(self, url=None, table_name=None, insert=False, update=False):
        self.url = url
        self.table_name = table_name
        self.insert = insert
        self.update = update
        self.columns = ColumnSettings()


class UserSettings(BaseCompatSetting):
    def __init__(self, csv=None, db=None, stop_on_error=False,
                 dbapi_verbose=False):
        if not csv:
            self.csv = CsvSettings()
        else:
            self.csv = csv
        if not db:
            self.db = DBSettings()
        else:
            self.db = db
        self.stop_on_error = stop_on_error
        self.dbapi_verbose = dbapi_verbose


def save_settings(path, settings):
    with open(path, "wb") as f:
        pickle.dump(settings, f, 0)
        f.flush()


def check_settings_valid(settings):
    assert isinstance(settings, UserSettings)
    assert isinstance(settings.csv, CsvSettings)
    assert isinstance(settings.db, DBSettings)
    assert isinstance(settings.db.columns, ColumnSettings)
    assert isinstance(settings.db.columns.val, list)
    for col in settings.db.columns.val:
        assert isinstance(col, ColumnSetting)


def check_csv2db_file_valid(path):
    try:
        with open(path, "rb") as f:
            settings = pickle.load(f)
            check_settings_valid(settings)
        return True, "", settings
    except Exception as e:
        logger.exception("csv2db file validity error")
        return False, unicode(e), None


class DataTypeMissing(Exception):
    pass

class CsvReadError(object):
    pass


class CsvImport(object):
    def __init__(self, settings):
        self.settings = settings

    def read_csv_line(self):
        csv_settings = copy.copy(self.settings.csv)
        columns = self.settings.db.columns.val
        for k in csv_settings.__dict__.keys():
            if k.startswith("_"):
                continue
            key = getattr(csv_settings, k)
            if key in char_mapping:
                setattr(csv_settings, k, char_mapping[key])
        delimiter = str(csv_settings.column_separator)
        quotechar = str(csv_settings.string_delimiter)
        counter = -1
        with open(csv_settings.file_path, "rU") as f:
            reader = csv.reader(f, delimiter=delimiter, quotechar=quotechar)
            for row in reader:
                counter += 1
                if csv_settings.skip_first_row and counter == 0:
                    continue
                line = []
                for idx, item in enumerate(row):
                    try:
                        coltype = columns[idx].column_type
                    except IndexError:
                        # most probably csv columns count does not equal columns defined in DB settings
                        exc_cls, exc, tb = sys.exc_info()
                        pill = CsvReadError()
                        pill.exc_cls = exc_cls
                        pill.exc = exc
                        pill.tb = tb
                        yield pill
                        break

                    if not coltype:
                        raise DataTypeMissing("Data type is missing for column '%s'" % columns[idx].column_name)
                    convfunc = column_types_dict[coltype]
                    if not item:
                        if columns[idx].default:
                            item = columns[idx].default
                    try:
                        val = convfunc(item, csv_settings)
                    except Exception as e:
                        exc_cls, exc, tb = sys.exc_info()
                        sys.stderr.write(unicode(e) + "\n")

                        pill = CsvReadError()
                        pill.exc_cls = exc_cls
                        pill.exc = exc
                        pill.tb = tb
                        yield pill
                        break

                    line.append(val)
                yield line


def detect_file_encoding(path):
    rawdata = open(path).read()
    result = chardet.detect(rawdata)
    return result

