# encoding: utf-8
from __future__ import unicode_literals, print_function, absolute_import

import sqlalchemy
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.engine import reflection


class DBImport(object):
    def __init__(self, dbapi, settings):
        self.dbapi = dbapi
        self.settings = settings
        self.table = None
        self.connection = None
        self.transaction = None
        self.pk_columns = []
        for col in self.settings.db.columns.val:
            if col.primary_key:
                self.pk_columns.append(col.column_name)
        self.repr_table()

    def start_transaction(self):
        self.connection = self.dbapi.engine.connect()
        self.transaction = self.connection.begin()

    def commit(self):
        self.transaction.commit()

    def rollback(self):
        self.transaction.rollback()

    def repr_table(self):
        self.table = sqlalchemy.Table(self.settings.db.table_name,
                                      self.dbapi.metadata,
                                      autoload=True,
                                      autoload_with=self.dbapi.engine)

    def add_record(self, rec):
        '''
        :param rec: dictionary of rec
        '''
        stmt = self.table.insert().values(**rec)
        res = self.connection.execute(stmt)
        return res.rowcount > 0

    def update_record(self, rec):
        '''
        :param rec: dictionary of rec
        '''
        uprec = {}
        where = []
        for k in rec.keys():
            if k not in self.pk_columns:
                uprec[k] = rec[k]
            else:
                where.append(getattr(self.table.c, k) == rec[k])
        stmt = self.table.update().values(**uprec).where(sqlalchemy.and_(*where))
        res = self.connection.execute(stmt)
        return res.rowcount > 0



class DBApi(object):
    def __init__(self, url, table_name=None, engine_kw=None, session_kw=None):
        self.url = url
        self.table_name = table_name
        self.engine_kw = engine_kw or {}
        self.engine = None
        self._session = None
        self.session_kw = session_kw or {}
        self.session_kw["autocommit"] = False  # just in case
        self._inspector = None
        self.metadata = sqlalchemy.MetaData()

    @property
    def session(self):
        if self._session is None:
            if self.engine is None:
                self.create_engine()
                self.create_session()
        return self._session

    @property
    def inspector(self):
        if self._inspector is None:
            if self.engine is None:
                self.create_engine()
            self._inspector = DBInspector(self.engine,
                                          table_name=self.table_name)
        return self._inspector

    def create_engine(self, **kw):
        self.engine_kw.update(kw)
        self.engine = sqlalchemy.create_engine(self.url, **self.engine_kw)
        self.metadata.bind = self.engine
        return self.engine

    def dispose_engine(self):
        if self.engine is not None:
            self.engine.dispose()

    def create_session(self, **kw):
        self.session_kw.update(kw)
        self._session = scoped_session(sessionmaker(**self.session_kw))
        if self.engine:
            self._session.configure(bind=self.engine)
        return self._session

    def close(self):
        if self._session is not None:
            self._session.close_all()
        self.dispose_engine()


class DBInspector(object):

    def __init__(self, engine, table_name=None):
        self.inspector = reflection.Inspector.from_engine(engine)
        self.table_name = table_name

    def get_table_names(self):
        '''
        [table 1, table 2, ... ]
        '''
        return self.inspector.get_table_names()


    def get_columns(self):
        """
        [{'primary_key': 1, 'nullable': False, 'default': None, 'autoincrement': True, 'type': INTEGER(), 'name': u'id'}, ...] 
        """
        return self.inspector.get_columns(self.table_name)


    def get_pk_constraint(self):
        """
        [col1,col2,...]
        """
        obj = self.inspector.get_pk_constraint(self.table_name)
        return obj.get("constrained_columns", [])
